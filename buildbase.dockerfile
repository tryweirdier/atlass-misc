ARG ALPINE_VERSION="latest"
FROM registry.gitlab.com/jitesoft/dockerfiles/alpine:${ALPINE_VERSION}

ENV PATH="/usr/lib/ccache/bin:$PATH" \
    CCACHE_COMPRESS="true" \
    CCACHE_MAXSIZE="256M"

ARG TARGETARCH

RUN apk add --no-cache --virtual .libs zlib libtool libgcc musl-dev openssl-dev zlib-dev libstdc++ libc-dev linux-headers \
 && apk add --no-cache --virtual .tools coreutils jq curl gnupg ccache wget grep tar xz autoconf dpkg file make re2c pkgconf automake \
 && apk add --no-cache --virtual .compilers gcc g++ ccache \
 && apk add --no-cache --virtual .misc ca-certificates \
 && UPX_VERSION=$(wget -qO- https://api.github.com/repos/upx/upx/releases | jq -r ".[0].tag_name") \
 && if [ "${TARGETARCH}" == "s390x" ]; then     \
        echo "#!/bin/sh" > /usr/bin/upx;        \
        echo "echo \"Dummy.\"" >> /usr/bin/upx; \
        echo "exit 0;" >> /usr/bin/upx;         \
        chmod +x /usr/bin/upx;                  \
        else                                    \
        DL_ARCH=$(case "${TARGETARCH}" in       \
          "amd64")   echo "amd64"       ;;      \
          "arm64")   echo "arm64"       ;;      \
          "386")     echo "i386"        ;;      \
          "arm")     echo "arm"         ;;      \
          "ppc64le") echo "powerpc64le" ;;      \
        esac);                                  \
        curl -sSL "https://github.com/upx/upx/releases/download/${UPX_VERSION}/upx-${UPX_VERSION#?}-${DL_ARCH}_linux.tar.xz" -o /tmp/upx.tar.xz; \
        mkdir -p /tmp/upx; \
        tar -xf /tmp/upx.tar.xz --strip-components 1 -C /tmp/upx; \
        cp /tmp/upx/upx /usr/bin; \
        rm -rf /tmp/up*; \
    fi
